# create Book using faker gem
faker_book = Faker::Book
10.times do 
	Book.create(title: faker_book.title, author: faker_book.author)
end