class BooksController < ApplicationController
	before_action :find_book, only: [:show]

	def index
		@book_lists = Book.all.order(:created_at)
	end

	def new
		@book = Book.new
	end

	def create
		@book = Book.new(book_params)
		if @book.save!
			redirect_to root_path
		else
			render @book
		end
	end

	def show
	end

	private
	def book_params
		params.require(:book).permit(:title, :author)
	end

	def find_book
		@book = Book.find(params[:id])
	end
end
